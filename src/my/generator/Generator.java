package my.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.lang.Math.max;

public class Generator {

    private final FileReader flReader;

    private String[] wordList;
    private char[] charList;

    private int[] firstCharRepartition;
    private int[][][] relativeCharRepartition;
    private int[] wordLengthRepartition;
    private int[][] relativeLastCharacter;

    private int maxWordLength;

    private final Random rand;

    public Generator() {
        logInfo("Create New Generator ....");
        flReader = new FileReader();
        wordList = flReader.getWordList();
        logInfo("Computing Data With The String Matrix ....\nPlease Wait ...");
        initCharList();
        logInfo("Characters Initialized Successfully !");
        initCharRepartition();
        logInfo("Computing Ended Successfully !");
        rand = new Random();
        logInfo("New Generator Created !");
    }

    private void initCharList() {
        ArrayList<Character> list = new ArrayList<>();
        maxWordLength = 0;
        for (String word : wordList) {
            maxWordLength = max(word.length(), maxWordLength);
            for (int i = 0; i < word.length(); i++) {
                char character = word.charAt(i);
                if (!list.contains(character))
                    list.add(character);
            }
        }
        charList = list.stream().map(String::valueOf).collect(Collectors.joining()).toCharArray();
        logInfo("Character Matrix Of " + charList.length + " Elements Ended !");
        Arrays.sort(charList);
        logInfo("Character Matrix Of " + charList.length + " Elements Sorted !");
    }

    private void initCharRepartition() {
        int length = charList.length;
        firstCharRepartition = new int[length];
        relativeCharRepartition = new int[length][length][length];
        wordLengthRepartition = new int[maxWordLength + 1];
        relativeLastCharacter = new int[length][length];
        for (String word : wordList) {
            firstCharRepartition[charListIndexOf(word.charAt(0))]++;
            int wordLength = word.length();
            for (int i = 0; i < wordLength - 1; i++)
                relativeCharRepartition
                        [charListIndexOf(word.charAt(max(0, i - 1)))]
                        [charListIndexOf(word.charAt(i))]
                        [charListIndexOf(word.charAt(i + 1))]
                        ++;
            relativeLastCharacter
                    [charListIndexOf(word.charAt(max(0, wordLength - 2)))]
                    [charListIndexOf(word.charAt(wordLength - 1))]
                    ++;
            wordLengthRepartition[wordLength]++;
        }
        logInfo("Matrix Of " + firstCharRepartition.length
                + " Elements Computing Ended !\nMatrix Of "
                + (relativeCharRepartition.length
                * relativeCharRepartition[0].length
                * relativeCharRepartition[0][0].length)
                + " Elements Computing Ended !\nMatrix Of "
                + wordLengthRepartition.length
                + " Elements Computing Ended !\nMatrix Of "
                + relativeLastCharacter.length * relativeLastCharacter[0].length
                + " Elements Computing Ended !"
        );
    }

    private int charListIndexOf(char chr) {
        Stream<Character> charStream = IntStream.range(0, charList.length).mapToObj(i -> charList[i]);
        ArrayList<Character> charListBuffer = (ArrayList<Character>) charStream.collect(Collectors.toList());
        return charListBuffer.indexOf(chr);
    }

    private int randomIndex(int[] probabilityTable) {
        logInfo("Selecting Random Index Of The Matrix " + probabilityTable
                + " Witch Contain " + probabilityTable.length + " Elements"
        );
        int total = 0;
        int length = probabilityTable.length;
        int maxIndex = length - 1;
        int[] ladder = new int[length];
        for (int i = 0; i < length; i++) {
            total += probabilityTable[i];
            ladder[i] = total;
        }
        if (total != 0) {
            int randomValue = rand.nextInt(total) + 1;
            int i = 0;
            while (i < maxIndex && randomValue > ladder[i])
                i++;
            return i;
        }
        return rand.nextInt(probabilityTable.length);
    }

    private char randomFirstLetter() {
        return charList[randomIndex(firstCharRepartition)];
    }

    private int randomLength() {
        logInfo("Selecting A Procedural Word Length ....");
        return randomIndex(wordLengthRepartition);
    }

    private char nextChar(char minus2, char minus1) {
        return charList[randomIndex(relativeCharRepartition[charListIndexOf(minus2)][charListIndexOf(minus1)])];
    }

    private char randomLastLetter(char chr) {
        return charList[randomIndex(relativeLastCharacter[charListIndexOf(chr)])];
    }

    private void logInfo(String message) {
        Logger.getLogger("Generator").log(Level.INFO, message);
    }

    public String generateWord() {
        StringBuilder word = new StringBuilder(String.valueOf(randomFirstLetter()));
        int length = randomLength();
        for (int i = 1; i < length - 1; i++)
            word.append(nextChar(word.charAt(max(0, i - 1)), word.charAt(i - 1)));
        word.append(randomLastLetter(word.charAt(max(0, length - 2))));
        return word.toString();
    }

    public String[] generateWords(int n) {
        String[] words = new String[n];
        for (int i = 0; i < n; i++)
            words[i] = generateWord();
        return words;
    }

    public void reload() {
        wordList = flReader.getWordList();
        initCharList();
        initCharRepartition();
    }

    public FileReader getFileReader() {
        return flReader;
    }
}
