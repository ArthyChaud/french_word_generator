package my.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileReader {

    private Scanner input;

    private final String pathToFile;
    private String fileName;

    private File file;

    public FileReader() {
        this("FrenchWords.txt");
    }

    public FileReader(String fileName) {

        Logger.getLogger("ReadFile").log(Level.INFO, "Reading The File ....");
        pathToFile = "assets" + File.separator;
        this.fileName = fileName;
        file = new File(pathToFile + fileName);
        initScanner();
    }

    private void initScanner() {
        try {
            input = new Scanner(file.getAbsoluteFile(), StandardCharsets.UTF_8.name());
            Logger.getLogger("ReadFile").log(Level.INFO, "File Convert Into String Matrix !");
        } catch (FileNotFoundException e) {
            Logger.getLogger("ReadFile").log(Level.SEVERE, "Error : Impossible To Load The File " + file);
            e.printStackTrace();
        }
    }

    public String[] getWordList() {
        ArrayList<String> wordList = new ArrayList<>();
        while (input.hasNextLine())
            wordList.add(input.nextLine());
        return wordList.toArray(new String[0]);
    }

    public void reload() {
        Logger.getLogger("ReadFile").log(Level.INFO, "Reading The File ....");
        file = new File(pathToFile + fileName);
        initScanner();
    }

    public void changeFileName(String fileName) {
        this.fileName = fileName;
        reload();
    }

    public void changeFile(File file) {
        this.file = file;
        initScanner();
    }
}
