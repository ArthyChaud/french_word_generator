package my.controllers.buttons;

import my.Interface;
import my.controllers.Controller;

import java.awt.event.ActionEvent;
import java.util.Arrays;

public class GenerateController extends Controller {
    public GenerateController(Interface vue) {
        super(vue);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int spinnerValue = (Integer) vue.getSpinner().getValue();
        vue.nbWordsGenerated += spinnerValue;
        int n = vue.nbWordsGenerated;
        vue.getDefaultListModel().addAll(Arrays.asList(vue.getGenerator().generateWords(spinnerValue)));
        vue.getLabel().setText("  " + n + " word" + (n > 1 ? "s" : "") + " generated  ");
        vue.getClear().setEnabled(true);
    }
}
