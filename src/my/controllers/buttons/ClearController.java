package my.controllers.buttons;

import my.Interface;
import my.controllers.Controller;

import java.awt.event.ActionEvent;

public class ClearController extends Controller {
    public ClearController(Interface vue) {
        super(vue);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        vue.getClear().setEnabled(false);
        vue.getDefaultListModel().removeAllElements();
        int nbClearedWords = vue.nbWordsGenerated;
        vue.nbWordsGenerated = 0;
        vue.getLabel().setText(nbClearedWords + " word" + (nbClearedWords > 1 ? "s" : "") + " cleared");
    }
}
