package my.controllers.buttons;

import my.generator.Generator;
import my.Interface;
import my.controllers.Controller;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class FileChooserController extends Controller {
    public FileChooserController(Interface vue) {
        super(vue);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.showOpenDialog(vue);
        Generator generator = vue.getGenerator();
        generator.getFileReader().changeFile(fileChooser.getSelectedFile().getAbsoluteFile());
        generator.reload();
    }
}
