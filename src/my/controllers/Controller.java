package my.controllers;

import my.Interface;

import java.awt.event.ActionListener;

public abstract class Controller implements ActionListener {
    protected Interface vue;

    protected Controller(Interface vue) {
        this.vue = vue;
    }
}
