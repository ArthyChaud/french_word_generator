package my.controllers;

import my.generator.Generator;
import my.Interface;

import java.awt.event.ActionEvent;

public class ComboBoxController extends Controller {

    public ComboBoxController(Interface vue) {
        super(vue);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Generator generator = vue.getGenerator();
        generator.getFileReader().changeFileName((String) vue.getComboBox().getSelectedItem());
        generator.reload();
    }
}
