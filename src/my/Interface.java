package my;

import my.controllers.buttons.ClearController;
import my.controllers.buttons.FileChooserController;
import my.controllers.buttons.GenerateController;
import my.controllers.ComboBoxController;
import my.generator.Generator;

import javax.swing.*;
import java.awt.*;

import static my.Commons.OPTIONS;

public class Interface extends JFrame {
    private final Generator generator;

    private JPanel mainPanel;

    private JComboBox<String> comboBox;
    private JSpinner spinner;
    private JButton generate;
    private JLabel label;
    private JButton clear;

    private DefaultListModel<String> dlm;

    public int nbWordsGenerated;

    public Interface() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        initComponents();
        addComponents();
        setPreferredSize(new Dimension(350, 400));
        setResizable(false);
        nbWordsGenerated = 0;
        pack();
        setVisible(true);
        generator = new Generator();
        label.setText("     Generator ready !      ");
        generate.setEnabled(true);
    }

    private void initComponents() {
        mainPanel = new JPanel();
        JPanel[] panels = new JPanel[5];
        for (int i = 0; i < panels.length; i++)
            panels[i] = new JPanel();
        comboBox = new JComboBox<>(OPTIONS);
        comboBox.setSelectedIndex(0);
        comboBox.addActionListener(new ComboBoxController(this));
        JButton fileChooserBut = new JButton("Select a file");
        fileChooserBut.addActionListener(new FileChooserController(this));
        spinner = new JSpinner(new SpinnerNumberModel(1, 1, Integer.MAX_VALUE, 1));
        generate = new JButton("Generate");
        generate.setEnabled(false);
        label = new JLabel("Loading the generator");
        dlm = new DefaultListModel<>();
        JList<String> list = new JList<>(dlm);
        generate.addActionListener(new GenerateController(this));
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(300, 175));
        clear = new JButton("Clear");
        clear.addActionListener(new ClearController(this));
        clear.setEnabled(false);
        panels[0].add(comboBox);
        panels[0].add(fileChooserBut);
        panels[1].add(spinner);
        panels[1].add(generate);
        panels[2].add(label);
        panels[3].add(listScroller);
        panels[4].add(clear);
        for (JPanel panel : panels)
            mainPanel.add(panel);
    }

    private void addComponents() {
        add(mainPanel);
    }

    public JComboBox getComboBox() {
        return comboBox;
    }

    public JSpinner getSpinner() {
        return spinner;
    }

    public JButton getGenerate() {
        return generate;
    }

    public JLabel getLabel() {
        return label;
    }

    public JButton getClear() {
        return clear;
    }

    public DefaultListModel<String> getDefaultListModel() {
        return dlm;
    }

    public Generator getGenerator() {
        return generator;
    }
}
